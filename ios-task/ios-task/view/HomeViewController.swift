//
//  HomeViewController.swift
//  ios-task
//
//  Created by khalifa on 8/16/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewControllerWithTableView<HomePresenterProtocol,Recipe,RecipeTableViewCell>,
UISearchBarDelegate {
    private static let RECIPE_CELL_NIB_NAME = "RecipeCell"
    private static let RECIPE_CELL_ID = "recipeCellIdentifiers"
    private static let SUGGESTIONS_CELL_ID = "suggestionsCell"
    private var suggestionsDataProvider: TableViewDataProvider<String?>?
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pagingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var suggestionsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
            searchBar.showsCancelButton = true
        }
    }
    @IBOutlet weak var recipesTableView: UITableView! {
        didSet {
            recipesTableView.dataSource = self
            recipesTableView.delegate = self
            recipesTableView.rowHeight = UITableViewAutomaticDimension;
            recipesTableView.estimatedRowHeight = 450
            recipesTableView.register(UINib(nibName :HomeViewController.RECIPE_CELL_NIB_NAME, bundle:nil),
                                      forCellReuseIdentifier: HomeViewController.RECIPE_CELL_ID)
        }
    }
    @IBOutlet weak var suggestionsListTableView: UITableView! {
        didSet {
            suggestionsListTableView.dataSource = self
            suggestionsListTableView.delegate = self
            suggestionsListTableView.estimatedRowHeight = 44
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == recipesTableView) {
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
        else {
            presenter?.onSuggestionsItemClicked(item: indexPath.item)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)-> Int {
        if(tableView == suggestionsListTableView) {
            return getNumberOfSuggestionsRows(forSection: section)
        }else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == suggestionsListTableView {
            return getSugeestionsCell(tableView:tableView,forRowAt:indexPath)
        } else {
            return super.tableView(tableView, cellForRowAt:indexPath)
        }
    }
    
    override func createPresenter()->HomePresenterProtocol {
        return HomePresenter(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            self.presenter?.searchButtonClicked(withText: text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchBarTextDidChanged(text: searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        presenter?.searchbarTextFinishedEditing()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        presenter?.searchbarTextStartedEditing()
    }
    
    override func getActivityIndicatorView()->  UIActivityIndicatorView? {
        return self.loadingIndicator
    }
    
    override func setCellData(cell: RecipeTableViewCell, item: Recipe?, indexPath: IndexPath) {
        cell.recipe = item
    }
    
    override func getCellId() -> String {
        return HomeViewController.RECIPE_CELL_ID
    }
    
    override func onLastItemWillbeDisplayed() {
        presenter?.lastRicepeWillBeDisplayed()
    }
    
    override func onItemSelected(item: Recipe?, indexPath: IndexPath) {
        presenter?.onRecipeItemSelected(item: item!, indexPath: indexPath)
    }
    override func getNoDataView() -> UIView? {
        return self.noDataView
    }
}

extension HomeViewController {
    private func getNumberOfSuggestionsRows(forSection section:Int) ->Int {
        return suggestionsDataProvider?.getNumberOfRows(inSection:section) ?? 0
    }
    
    private func getSugeestionsCell(tableView:UITableView,
                                    forRowAt indexPath: IndexPath)->UITableViewCell {
        let cell  = tableView.dequeueReusableCell(
            withIdentifier: HomeViewController.SUGGESTIONS_CELL_ID, for: indexPath)
        cell.textLabel?.text = suggestionsDataProvider?.getItem(at: indexPath.item)
        return cell
    }
    
}


extension HomeViewController: HomeViewProtocol {
    func setSearchButtonText(text: String) {
        searchBar.text = text
    }
    
    func pushViewController(viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setupSuggestionsTableView(provider: TableViewDataProvider<String?>) {
        self.suggestionsDataProvider = provider
    }
    
    func setupRecipesTableView(provider: TableViewDataProvider<Recipe>) {
        onProviderAttached(provider: provider)
    }
    
    func reloadSuggestionsList() {
        suggestionsListTableView.reloadData()
        updateSuggestionsTableViewHeight()
    }
    
    private func updateSuggestionsTableViewHeight() {
        suggestionsHeightConstraint.constant = suggestionsListTableView
            .estimatedRowHeight * CGFloat(
                (suggestionsDataProvider?.getNumberOfRows(inSection: 0) ?? 0 ))
    }
    
    
    func removeSearchFieldFocus() {
        searchBar?.resignFirstResponder()
    }
    
    func reloadRecipesList() {
        self.recipesTableView.reloadData()
    }
    
    func showPaginationIndicator() {
        pagingIndicator?.startAnimating()
    }
    
    func hidePaginationIndicator() {
        pagingIndicator?.stopAnimating()
    }
    
    func showSuggestionsList() {
        suggestionsListTableView.isHidden = false
    }
    
    func hideSuggestionsList() {
        suggestionsListTableView.isHidden = true
    }
}
