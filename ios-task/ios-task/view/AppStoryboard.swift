//
//  AppStoryboard.swift
//  ios-task
//
//  Created by khalifa on 8/28/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit
enum AppStoryboard : String {
    case Main, RecipeDetails
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type)->T {
        let storyBoardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyBoardID) as! T
    }
    
    func instantiateInitialViewController()->UIViewController? {
        return instance.instantiateInitialViewController()
    }
}
