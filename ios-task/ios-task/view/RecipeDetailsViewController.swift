//
//  RecipeDetailsViewController.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit
import SafariServices
class RecipeDetailsViewController: BaseViewController<RecipeDetailsPresenterProtocol>,
RecipeDetailsScreenProtocol,UIScrollViewDelegate,UITextViewDelegate {
    private static let DEFAULT_ID_VALUE = "-1"
    var recipeId:String = RecipeDetailsViewController.DEFAULT_ID_VALUE
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var loadingViewIndicator: UIActivityIndicatorView!
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var recipeLinkTextView: UITextView! {
        didSet {
            recipeLinkTextView.isUserInteractionEnabled = true
            recipeLinkTextView.delegate = self
        }
    }
    @IBOutlet weak var mainScrollView: UIScrollView! {
        didSet {
            mainScrollView.delegate = self
        }
    }
    
    func startSFSafariViewController(url: URL) {
        let viewController = SFSafariViewController(url: url)
        self.present(viewController, animated: true){}
    }
    
    func showRecipeImage(fromURL url: String) {
        guard let imageURL = URL(string: url) else {return}
        guard NetworkUtilities.isConnectedToNetwork() else {return}
        NetworkUtilities.downloadImage(url: imageURL) {
            image, error in
            if let validImage = image {
                self.recipeImageView.image = validImage
            }
        }
    }
    
    func setRecipeTitle(title: String) {
        titleLabel.text = title
    }
    
    func setRecipePublisherLink(text: NSMutableAttributedString) {
        recipeLinkTextView.attributedText = text
    }
    
    func setIngrediantsText(text: String) {
        ingredientsLabel.text = text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad(withRecipeId: recipeId)
    }
    
    override func createPresenter() -> RecipeDetailsPresenterProtocol? {
        return RecipeDetailsPresenter(view: self)
    }
    
    override func getActivityIndicatorView() -> UIActivityIndicatorView? {
        return loadingViewIndicator
    }
    
    override func getNoDataView() -> UIView? {
        return noDataView
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        presenter?.onRecipeLinkClicked()
        return false
    }
}

