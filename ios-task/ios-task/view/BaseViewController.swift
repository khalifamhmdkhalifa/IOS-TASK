//
//  BaseViewController.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

class BaseViewController<P> : UIViewController
{
    private let NO_NETWORK_MESSAGE = "you are not connected to any network !"
    private let NO_NETWORK_TITLE = "Warning"
    private let NO_NETWORK_CANCEL_TEXT = "Ok"
    private(set) var presenter: P?
    
    private func getPresenter()-> BasePresenterProtocol?
    {
        return presenter as? BasePresenterProtocol
    }
    
    func createPresenter() -> P?
    {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = createPresenter()
        getPresenter()?.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        getPresenter()?.viewWillLayoutSubviews()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        getPresenter()?.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPresenter()?.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) { 
        super.viewDidAppear(animated)
        getPresenter()?.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        getPresenter()?.viewWillDisAppear()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        getPresenter()?.viewDidDisAppear()
    }
    
    func getActivityIndicatorView()->  UIActivityIndicatorView?
    {
        return nil
    }
    
    
    
    func getNoDataView()->UIView?
    {
        return nil
    }
    
    func showNoInternetAlert(){
        showWarningMessage(title:NO_NETWORK_TITLE , message:NO_NETWORK_MESSAGE , cancelText: NO_NETWORK_CANCEL_TEXT)
    }
    
    
    
}

extension UIViewController {
    static func instatiateFromAppStoryBoard(appStoryBoard: AppStoryboard)-> Self
    {
        return appStoryBoard.viewController(viewControllerClass:self)
    }
    class var storyboardID : String {
        return "\(self)"
    }
}


extension BaseViewController: BaseViewControllerProtocol
{
    
    func showWarningMessage(title:String,message:String,cancelText:String)
    {
        let alert = UIAlertController.init(title:title , message:message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:cancelText , style: .cancel){ action in
            alert.dismiss(animated: true, completion: nil)
        })
        self.present(alert, animated: true,completion: nil)
    }
    
    func showNoDataView() {
        getNoDataView()?.isHidden = false
    }
    
    func hideNoDataView() {
        getNoDataView()?.isHidden = true
    }
    
    func showLoadingIndicator() {
        getActivityIndicatorView()?.startAnimating()
    }
    
    func hideLoadingIndicator() {
        self.getActivityIndicatorView()?.stopAnimating()
    }
}
