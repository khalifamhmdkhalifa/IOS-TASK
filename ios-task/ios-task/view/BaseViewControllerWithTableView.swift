//
//  BaseViewControllerWithTableView.swift
//  ios-task
//
//  Created by khalifa on 8/19/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit


class BaseViewControllerWithTableView<PRESENTER,ITEM,CELL:UITableViewCell>: BaseViewController<PRESENTER>,
    UITableViewDataSource,UITableViewDelegate
{
    private(set) var mainTableDataProvider :TableViewDataProvider<ITEM>?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainTableDataProvider?.getNumberOfRows(inSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: getCellId(),
                                                 for: indexPath)
        setCellData(cell: cell as! CELL,
                    item: (mainTableDataProvider?.getItem(at: indexPath.item))!,
                    indexPath:indexPath)
        return cell
    }
    
    func onProviderAttached(provider: TableViewDataProvider<ITEM>)
    {
        self.mainTableDataProvider  = provider
    }
    
    func setCellData(cell:CELL,item:ITEM,indexPath:IndexPath)
    {
        return
    }
    
    func getCellId()->String
    {
        return ""
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        if let count = mainTableDataProvider?.getNumberOfRows(inSection: indexPath.section),
            count == indexPath.item + 1
        {
            onLastItemWillbeDisplayed()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onItemSelected(item: mainTableDataProvider?.getItem(at: indexPath.item),
                       indexPath: indexPath)
    }
    
    func onLastItemWillbeDisplayed()
    {
    }
    
    func onItemSelected(item:ITEM?, indexPath:IndexPath)
    {
    }
}
