//
//  RecipeDetailsScreenProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol RecipeDetailsScreenProtocol: BaseViewControllerProtocol
{
    func showRecipeImage(fromURL url: String)
    func setRecipeTitle(title: String)
    func setRecipePublisherLink(text: NSMutableAttributedString)
    func setIngrediantsText(text: String)
    func startSFSafariViewController(url: URL)
}
