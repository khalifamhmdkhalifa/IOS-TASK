//
//  BaseViewControllerProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol BaseViewControllerProtocol {
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func showNoDataView()
    func hideNoDataView()
    func showWarningMessage(title:String,message:String,cancelText:String)
    func showNoInternetAlert()
}
