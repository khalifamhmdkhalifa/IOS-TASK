//
//  MainScreenProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/16/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit
protocol HomeViewProtocol: BaseViewControllerProtocol,PaginationViewController
{
    func removeSearchFieldFocus()
    func showSuggestionsList()
    func hideSuggestionsList()
    func reloadSuggestionsList()
    func reloadRecipesList()
    func setupRecipesTableView(provider: TableViewDataProvider<Recipe>)
    func setupSuggestionsTableView(provider: TableViewDataProvider<String?>)
    func pushViewController(viewController: UIViewController)
    func setSearchButtonText(text: String)
}
