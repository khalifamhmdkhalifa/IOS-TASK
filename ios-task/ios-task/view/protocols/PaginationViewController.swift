//
//  PaginationViewController.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol PaginationViewController {
    func showPaginationIndicator()
    func hidePaginationIndicator()
}
