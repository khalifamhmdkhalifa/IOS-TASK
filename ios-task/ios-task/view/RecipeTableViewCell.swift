//
//  RecipeTableViewCell.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

class RecipeTableViewCell: UITableViewCell {
    private static let RATING_VALUE_FORMAT = "%.1f"
    private static let RATING_DEFAULT_VALUE = "0"
    let defaultImage:UIImage? = UIImage(named:"defaultImage")
    @IBOutlet weak var recipeRatingLabel: UILabel!
    @IBOutlet weak var recipePublisherLabel: UILabel!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    var recipe:Recipe? {
        didSet {
            updateCellInfo(recipe: recipe)
        }
    }
    
    private func updateCellInfo(recipe: Recipe?) {
        guard let newValue = recipe else {return}
        handleSettingTheImage(url: newValue.imageURL ?? "")
        recipeTitleLabel.text = newValue.title ?? ""
        if let rate = newValue.socialRank {
            recipeRatingLabel.text = String(format: RecipeTableViewCell.RATING_VALUE_FORMAT , rate)
        } else {
            recipeRatingLabel.text = RecipeTableViewCell.RATING_DEFAULT_VALUE
        }
        recipePublisherLabel.text = newValue.publisher ?? ""
    }
    
    private func handleSettingTheImage(url:String) {
        recipeImage.image = defaultImage
        guard NetworkUtilities.isConnectedToNetwork(),
            let validUrl = URL(string:url) else {return}
        downloadImage(url: validUrl)
    }
    
    func downloadImage(url: URL) {
        NetworkUtilities.downloadImage(url: url){ image, error in
            if url.absoluteString == self.recipe?.imageURL {
                self.recipeImage?.image = image
            }else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}
