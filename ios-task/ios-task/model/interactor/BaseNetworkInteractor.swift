//
//  BaseNetworkInteractor.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
class BaseNetworkInteractor {
    
    func  loadData<T:Decodable>(fromURL url:String,onWorkDone:@escaping (T?,Error?)->Void){
        guard let url = URL(string: url) else {
            onWorkDone(nil,BaseError(errorMessage: "Bad URL"))
            return
        }
        NetworkUtilities.getDataFromUrl(url: url){ data,response,error in
            guard let data = data else{
                print(error.debugDescription)
                onWorkDone(nil,error)
                return
            }
            
            do{
                let result = try JSONDecoder().decode(T.self, from: data)
                onWorkDone(result,error)
            }catch let decodeError {
                onWorkDone(nil,decodeError)
            }
        }
    }
    
    struct BaseError :Error{
        let errorMessage:String
    }
}
