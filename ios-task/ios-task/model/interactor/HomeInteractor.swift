//
//  HomeInteractor.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
class HomeInteractor:BaseNetworkInteractor, HomeInteractorProtocol
{
    var isLoading: Bool = false
    var isEndReached: Bool = false
    var currentPage: Int = 0
    private var currentQuery = ""
    private static let RECIPE_SEARCH_URL = "https://food2fork.com/api/search?key=158d5a19765152ac5d7449d14a67c684"
    private static let SEARCH_WORD_KEY = "q"
    private static let SEARCH_PAGE_KEY = "page"
    static var maxSavedSuggestions: Int = 10
    private static let SUGGESTIONS_KEY = "suggestion"
    private var tempSuggestionsList: [String]?
    private var loadedReciped = [Recipe]()
    private var suggestionsList:[String]{
        get{
            guard  tempSuggestionsList != nil
                else {
                    tempSuggestionsList = UserDefaults.standard.stringArray(
                        forKey:HomeInteractor.SUGGESTIONS_KEY)
                    return tempSuggestionsList ?? [String]()
            }
            return tempSuggestionsList!
        }
        set{
            tempSuggestionsList = nil
            UserDefaults.standard.setValue(newValue, forKey:HomeInteractor.SUGGESTIONS_KEY)
        }
    }
    
    func clearLoadedRecipes() {
        loadedReciped.removeAll()
    }
    
    func loadNextPage(onDataLoaded: @escaping ([Recipe]?, Error?) -> Void) {
        isLoading = true
        loadNextPageResponse(){ response,error in
            self.onResponseResult(response: response)
            onDataLoaded(response?.recipes,error)
        }
    }
    
    func performSearch(forText text :String,
                       onDataLoaded: @escaping ([Recipe]?,Error?) -> Void) {
        currentQuery = text
        currentPage = 0
        isLoading = true
        isEndReached = false
        loadNextPageResponse(){ response,error in
            self.onResponseResult(response: response)
            onDataLoaded(response?.recipes,error)
        }
    }
    
    private func isValidSearchResponse(response: SearchResponse? )->Bool {
        return response != nil && response?.recipes != nil
    }
    
    private func checkIfEndReached(response:SearchResponse) {
        isEndReached =  response.count == 0
    }
    
    private func onResponseResult(response:SearchResponse?) {
        isLoading = false
        if isValidSearchResponse(response:response) {
            currentPage += 1
            checkIfEndReached(response: response!)
            if response!.recipes != nil {
                self.loadedReciped.append(contentsOf: response!.recipes!)
            }
        }
    }
    
    private func loadNextPageResponse(onDataLoaded: @escaping (SearchResponse?,Error?) -> Void) {
        let nextPage = currentPage + 1
        loadData(fromURL: getValidSearchURL(
            forText:currentQuery, page:nextPage),onWorkDone: onDataLoaded)
    }
    
    private func getValidSearchURL(forText text:String,page: Int)->String {
        let encodedKey = text.addingPercentEncoding(
            withAllowedCharacters: .urlHostAllowed) ?? text
        let result = "\(HomeInteractor.RECIPE_SEARCH_URL)" +
            "&\(HomeInteractor.SEARCH_WORD_KEY)=\(encodedKey)" +
        "&\(HomeInteractor.SEARCH_PAGE_KEY)=\(page)"
        return result
    }
    
    func getLoadedRecipes() -> [Recipe]? {
        return loadedReciped
    }
    
    func loadLatestSuggestions(onSuccess: ([String]) -> Void) {
        onSuccess(getSuggestions())
    }
    
    func addNewSuggestion(_ value:String, onFinish: (_ isAdded: Bool) -> Void) {
        var suggestoions = suggestionsList
        guard(!suggestoions.contains(value))else {
            onFinish(false)
            return;
        }
        
        if(suggestoions.count >= HomeInteractor.maxSavedSuggestions) {
            var newSuggestions = suggestoions[0..<(HomeInteractor.maxSavedSuggestions-1)]
            newSuggestions.insert(value, at: 0)
            suggestionsList = Array(newSuggestions)
        }else {
            suggestoions.insert(value, at: 0)
            self.suggestionsList = suggestoions
        }
        onFinish(true)
    }
    
    private func getSuggestions()->[String] {
        return suggestionsList
    }
    
}
