//
//  RecipeDetailsInteractorProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol RecipeDetailsInteractorProtocol
{
    var recipe:Recipe?{get}
    func loadRecipeDetails(
        forRecipeId id:String,onComplete: @escaping (Recipe?,Error?)->Void)
}
