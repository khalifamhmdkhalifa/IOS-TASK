//
//  HomeInteractorProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation

protocol HomeInteractorProtocol: PagingInteractorProtocol
{
    static var maxSavedSuggestions:Int{get set}
    func loadLatestSuggestions(onSuccess:(_ suggestions:[String])->Void)
    func addNewSuggestion(_ value:String,onFinish:(_ isAdded:Bool)->Void)
    func getLoadedRecipes()->[Recipe]?
    func loadNextPage(onDataLoaded:@escaping ([Recipe]?,Error?)->Void)
    func performSearch(forText text :String ,
                       onDataLoaded: @escaping ([Recipe]?,Error?) -> Void)->Void
    func clearLoadedRecipes()
}
