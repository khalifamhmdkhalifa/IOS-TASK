//
//  PagingInteractorProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation

protocol PagingInteractorProtocol {
    var currentPage:Int{get}
    var isEndReached:Bool{get}
    var isLoading:Bool{get}
}
