//
//  RecipeDetailsInteractor.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation

class RecipeDetailsInteractor : BaseNetworkInteractor, RecipeDetailsInteractorProtocol
{
    var recipe: Recipe?
    private static let URL = "https://food2fork.com/api/get?key=158d5a19765152ac5d7449d14a67c684"
    private static let ID_KEY = "rId"
    
    func loadRecipeDetails(forRecipeId id: String, onComplete: @escaping (Recipe?, Error?) -> Void) {
        loadData(fromURL: getValidUrl(forId: id)){ (respone: RecipeDetailsResponse? ,error:Error?) in
            self.recipe = respone?.recipe
            onComplete(self.recipe, error)
        }
    }
    
    private func getValidUrl(forId id: String)->String {
        return "\(RecipeDetailsInteractor.URL)&\(RecipeDetailsInteractor.ID_KEY)=\(id)"
    }
}
