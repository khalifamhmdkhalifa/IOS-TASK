//
//  TableViewDataProvider.swift
//  ios-task
//
//  Created by khalifa on 8/19/18.
//  Copyright © 2018 khalifa. All rights reserved.
//
import Foundation

class TableViewDataProvider<T>: BaseTableViewDataProviderProtocol {
    typealias Item = T
    private let numberOfSections: ()->Int
    private let numberOfRowsForSection:(Int)->Int
    private let getItem:(Int)->Item
    
    init(numberOfSections:@escaping ()->Int,
         numberOfRowsForSection:@escaping (Int)->Int,
         getItem:@escaping (Int)->Item) {
        self.getItem = getItem
        self.numberOfSections = numberOfSections
        self.numberOfRowsForSection = numberOfRowsForSection
    }
    func getNumberOfRows(inSection section: Int) -> Int {
        return self.numberOfRowsForSection(section)
    }
    
    func getNumberOfSections() -> Int {
        return self.numberOfSections()
    }
    
    func getItem(at position: Int) -> Item {
        return self.getItem(position)
        
    }
}
