//
//  RecipeDetailsResponse.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
struct RecipeDetailsResponse:Decodable {
    var recipe:Recipe
}
