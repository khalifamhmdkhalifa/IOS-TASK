//
//  SearchResponse.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
struct SearchResponse: Decodable
{
    var recipes:[Recipe]?
    var count:Int
}
