//
//  BaseTableViewDataSource.swift
//  ios-task
//
//  Created by khalifa on 8/19/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol BaseTableViewDataProviderProtocol:AnyObject {
    associatedtype Item
    func getNumberOfRows(inSection section: Int)->Int
    func getNumberOfSections()-> Int
    func getItem(at indexh:Int)-> Item
}
