//
//  Recipe.swift
//  ios-task
//
//  Created by khalifa on 8/17/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
struct Recipe: Decodable 
{
    var publisher:String?
    var webURL:String?
    var ingredients:[String]?
    var sourceUrl:String?
    var recipeId:String?
    var imageURL:String?
    var title:String?
    var publisherURL:String?
    var socialRank:Double?
    
    enum CodingKeys : String, CodingKey {
        case publisher = "publisher"
        case webURL = "f2f_url"
        case sourceUrl = "source_url"
        case recipeId = "recipe_id"
        case imageURL = "image_url"
        case title = "title"
        case publisherURL = "publisher_url"
        case socialRank = "social_rank"
        case ingredients = "ingredients"
    }
    
    
}
