//
//  BasePresenter.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
class BasePresenter<V,I> :BasePresenterProtocol
{
    var view:V
    private(set) var interactor:I?
    
    func createInteractor()->I?{
        return nil
    }
    func viewDidLoad() {
        
    }
    
    func viewWillLayoutSubviews() {
        
    }
    
    func viewDidLayoutSubviews() {
        
    }
    
    func viewWillAppear() {
        
    }
    
    func viewDidAppear() {
        
    }
    
    func viewWillDisAppear() {
        
    }
    
    func viewDidDisAppear() {
        
    }
    
    init(view:V){
        self.view = view
        self.interactor = createInteractor()
    }
}
