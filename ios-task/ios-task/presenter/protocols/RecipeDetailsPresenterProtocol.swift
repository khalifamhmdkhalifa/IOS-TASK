//
//  RecipeDetailsPresenterProtocol.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol RecipeDetailsPresenterProtocol: BasePresenterProtocol {
    func viewDidLoad(withRecipeId recipeId:String)
    func onRecipeLinkClicked()
}
