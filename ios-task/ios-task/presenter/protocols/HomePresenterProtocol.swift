//
//  MainScreenPresenter.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
protocol HomePresenterProtocol: BasePresenterProtocol {
    func searchButtonClicked(withText text: String)
    func searchBarTextDidChanged(text: String)
    func searchbarTextFinishedEditing()
    func searchbarTextStartedEditing()
    func lastRicepeWillBeDisplayed()
    func onRecipeItemSelected(item: Recipe, indexPath: IndexPath)
    func onSuggestionsItemClicked(item:Int)
}
