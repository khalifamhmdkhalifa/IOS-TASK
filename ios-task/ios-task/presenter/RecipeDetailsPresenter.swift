//
//  RecipeDetailsPresenter.swift
//  ios-task
//
//  Created by khalifa on 8/18/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
class RecipeDetailsPresenter: BasePresenter<RecipeDetailsScreenProtocol, RecipeDetailsInteractorProtocol>{
    private static let RECIPE_LINK_TEXT_VALUE = "Visit Recipe Web Page"
    private static let DEFAULT_ID = "-1"
    
    private func loadRecipe(id:String){
        view.hideNoDataView()
        view.showLoadingIndicator()
        interactor?.loadRecipeDetails(forRecipeId: id){[weak self]
            recipe,error in
            self?.view.hideLoadingIndicator()
            if let recipe = recipe {
                self?.updateShownRecipeData(recipe: recipe)
            }else {
                self?.view.showNoDataView()
                
            }
        }
    }
    
    private func updateShownRecipeData(recipe:Recipe){
        if let url = recipe.imageURL{
            view.showRecipeImage(fromURL:url)
        }
        
        if let ingradiants  = recipe.ingredients, ingradiants.count > 0{
            view.setIngrediantsText(text: getValidIngradiantText(ingradiants: ingradiants))
        }
        if let title = recipe.title {
            view.setRecipeTitle(title: title)
        }
        
        if let link = recipe.sourceUrl {
            setShownRecipeLink(link: link)
        }
    }
    private func setShownRecipeLink(link:String){
        let attributedText = NSMutableAttributedString(string:RecipeDetailsPresenter.RECIPE_LINK_TEXT_VALUE)
        attributedText.addAttribute(.link,
                                    value: link,
                                    range: NSRange(location: 0, length: attributedText.length))
        view.setRecipePublisherLink(text: attributedText)
    }
    
    func getValidIngradiantText(ingradiants: [String])->String{
        let separator = "\n"
        var result = ""
        for value in ingradiants{
            result += value + separator
        }
        result.removeLast(separator.count)
        return result
    }
    
    override func createInteractor() -> RecipeDetailsInteractorProtocol? {
        return RecipeDetailsInteractor()
    }
}


extension RecipeDetailsPresenter:RecipeDetailsPresenterProtocol{
    func viewDidLoad(withRecipeId recipeId: String) {
        if recipeId == RecipeDetailsPresenter.DEFAULT_ID {
            view.showNoDataView()
            return
        }
        loadRecipe(id: recipeId)
    }
    
    func onRecipeLinkClicked() {
        if let url = interactor?.recipe?.sourceUrl, let validUrl = URL(string: url){
            view.startSFSafariViewController(url: validUrl)
        }
    }
    
}


