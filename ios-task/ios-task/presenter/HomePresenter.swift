//
//  mainScreenPresenter.swift
//  ios-task
//
//  Created by khalifa on 8/15/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import Foundation
class HomePresenter: BasePresenter<HomeViewProtocol,HomeInteractorProtocol> {
    
    private var currentSearchText:String?
    private static let NO_SEARCH_RESULT_MESSAGE = "No Recipes Found !"
    private static let NO_SEARCH_RESULT_WARNING_CANCEL_TEXT = "Ok"
    
    func getSuggestions(forText text: String?) -> [String] {
        var result:[String] = [String]()
        interactor?.loadLatestSuggestions(){ suggestions in
            result = suggestions
        }
        if let textValue = text, !textValue.isEmpty {
            return result.filter(){$0.contains(text!)}
        }
        return result
    }
    
    override func createInteractor() -> HomeInteractorProtocol? {
        return HomeInteractor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRecipesView()
        setupSuggestionsView()
    }
    
    private func setupRecipesView(){
        let dataProvider = TableViewDataProvider<Recipe>(numberOfSections: {
            return 1
        }, numberOfRowsForSection: { [weak self] (section) -> Int in
            return self?.interactor?.getLoadedRecipes()?.count ?? 0
        }) {[weak self] (index) ->Recipe in
            return self!.interactor!.getLoadedRecipes()![index]
        }
        view.setupRecipesTableView(provider: dataProvider)
    }
    
    private func setupSuggestionsView(){
        let dataProvider = TableViewDataProvider<String?>(numberOfSections: {
            return 1
        }, numberOfRowsForSection: { [weak self] (section) -> Int in
            return self?.getSuggestions(forText: self?.currentSearchText).count ?? 0
        }) {[weak self] (index) -> String? in
            return self?.getSuggestions(forText: self?.currentSearchText)[index]
        }
        view.setupSuggestionsTableView(provider: dataProvider)
    }
}


extension HomePresenter: HomePresenterProtocol {
    private func handleSuccessfullSearchResponse(recipes:[Recipe]) {
        if(recipes.count > 0 ){
            view.reloadRecipesList()
        }else{
            handleEmptySearchResult()
           }
    }
    
    private func handleEmptySearchResult(){
        view.showNoDataView()
        view.showWarningMessage(title: "",
                                message: HomePresenter.NO_SEARCH_RESULT_MESSAGE ,
                                cancelText: HomePresenter.NO_SEARCH_RESULT_WARNING_CANCEL_TEXT)
    }
    
    func onSuggestionsItemClicked(item: Int) {
        interactor?.loadLatestSuggestions(){ [weak self] suggestions in
            guard suggestions.count > item else{return}
            
            let selectedText = suggestions[item]
            self?.currentSearchText = selectedText
            self?.view.setSearchButtonText(text: selectedText)
            self?.view.hideSuggestionsList()
            self?.view.removeSearchFieldFocus()
            self?.searchButtonClicked(withText: selectedText)
        }
    }
    
    func onRecipeItemSelected(item: Recipe, indexPath: IndexPath) {
        guard NetworkUtilities.isConnectedToNetwork() else{
            view.showNoInternetAlert()
            return
        }
        let detailsViewController = RecipeDetailsViewController
            .instatiateFromAppStoryBoard(appStoryBoard: AppStoryboard.RecipeDetails)
        detailsViewController.recipeId = item.recipeId!
        view.pushViewController(viewController: detailsViewController)
    }
    
    func searchbarTextStartedEditing() {
        interactor?.loadLatestSuggestions(){suggestios in
            if(suggestios.isEmpty ){
                view.hideSuggestionsList()
                return
            }else {
                view.showSuggestionsList()
                view.reloadSuggestionsList()
            }
        }
    }
    
    func searchbarTextFinishedEditing() {
        view.hideSuggestionsList()
    }
    
    func lastRicepeWillBeDisplayed() {
        guard NetworkUtilities.isConnectedToNetwork() else{
            return
        }
        
        if let interactor = self.interactor , (interactor.isLoading || interactor.isEndReached){
            return
        }
        view.showPaginationIndicator()
        interactor?.loadNextPage(onDataLoaded: ){
            recipes , error in
            self.view.hidePaginationIndicator()
            self.view.reloadRecipesList()
        }
    }
    
    func getLoadedRecipes() -> [Recipe]? {
        return interactor?.getLoadedRecipes()
    }
    
    func searchButtonClicked(withText text: String) {
        guard NetworkUtilities.isConnectedToNetwork() else{
            view.showNoInternetAlert()
            return
        }
        guard let interactor = self.interactor, !interactor.isLoading else {return}
        view.removeSearchFieldFocus()
        interactor.clearLoadedRecipes()
        view.reloadRecipesList()
        view.hideNoDataView()
        view.showLoadingIndicator()
        interactor.performSearch(forText: text ){ [weak self] (recipes, error) in
            self?.onSearchResponse(recipes: recipes, error: error)
        }
        guard !text.isEmpty else {return}
        interactor.addNewSuggestion(text)
        { isDone in
            if isDone{
                view.reloadSuggestionsList()
            }
        }
    }
    
    private func onSearchResponse(recipes:[Recipe]?,error:Error?) {
        self.view.hideLoadingIndicator()
        if let recipes = recipes {
            self.handleSuccessfullSearchResponse(recipes: recipes)
        } else {
            self.view.showNoDataView()
        }
    }
    
    func searchBarTextDidChanged(text: String) {
        currentSearchText = text
        interactor?.loadLatestSuggestions(){suggestions in
            if(suggestions.isEmpty ){
                view.hideSuggestionsList()
            }else{
                view.reloadSuggestionsList()
            }
        }
    }
}
